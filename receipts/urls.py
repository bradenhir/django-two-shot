from django.urls import path
from receipts.views import (
    receipt_list_view,
    create_receipt,
    expense_category_list_view,
    account_list_view,
    create_category,
    create_account,
)


urlpatterns = [
    path("", receipt_list_view, name="home"),
    path("create/", create_receipt, name="create_receipt"),
    path("categories/", expense_category_list_view, name="category_list"),
    path("accounts/", account_list_view, name="account_list"),
    path("categories/create/", create_category, name="create_category"),
    path("accounts/create/", create_account, name="create_account"),
]
