from django.shortcuts import render, redirect
from receipts.models import Receipt, ExpenseCategory, Account
from django.contrib.auth.decorators import login_required
from receipts.forms import ReceiptForm, ExpenseForm, AccountForm


# Create your views here.
@login_required
def receipt_list_view(request):
    allreceipts = Receipt.objects.filter(purchaser=request.user)
    context = {
        "allreceipts": allreceipts,
    }
    return render(request, "receipts/list.html", context)


@login_required
def expense_category_list_view(request):
    allcategories = ExpenseCategory.objects.filter(owner=request.user)
    context = {
        "allcategories": allcategories,
    }
    return render(request, "receipts/category-list.html", context)


@login_required
def account_list_view(request):
    allaccounts = Account.objects.filter(owner=request.user)
    context = {
        "allaccounts": allaccounts,
    }
    return render(request, "receipts/account-list.html", context)


@login_required
def create_receipt(request):
    if request.method == "POST":
        form = ReceiptForm(request.POST)
        if form.is_valid():
            receipt = form.save(False)
            receipt.purchaser = request.user
            form.save()
            return redirect("home")  # id=receipt.id )

    else:
        form = ReceiptForm()
    context = {"form": form}
    return render(request, "receipts/create.html", context)


@login_required
def create_category(request):
    if request.method == "POST":
        form = ExpenseForm(request.POST)
        if form.is_valid():
            category = form.save(False)
            category.owner = request.user
            form.save()
            return redirect("category_list")
    else:
        form = ExpenseForm()
    context = {
        "form": form,
    }
    return render(request, "receipts/create-expense-category.html", context)


@login_required
def create_account(request):
    if request.method == "POST":
        form = AccountForm(request.POST)
        if form.is_valid():
            account = form.save(False)
            account.owner = request.user
            form.save()
            return redirect("account_list")
    else:
        form = AccountForm()
    context = {"form": form}
    return render(request, "receipts/create-account.html", context)
